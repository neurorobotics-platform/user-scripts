function mysplit (inputstr, sep)
        if sep == nil then
                sep = "%s"
        end
        local t={}
        for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
                table.insert(t, str)
        end
        return t
end

function get_backend(txn)
    local my_path = txn.sf:path()
    local mytbl = mysplit(my_path,"/")
    return tostring(mytbl[1])
end

core.register_fetches("get_backend", get_backend) 
